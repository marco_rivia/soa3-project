using System;
using System.Reflection.Metadata;
using soa3_project;
using Xunit;

namespace soa3_project.tests
{
    public class OrderTest
    {

        private DateTime WeekDay = new DateTime(2022, 1, 3);
        private DateTime Weekend = new DateTime(2022, 1, 1);

        [Fact]
        public void Test_Student_Order_Returns_Correct_Value()
        {
            int regularTickets = 2;
            int premiumTickets = 2;

            Order order = new Order(1, true);
            double res = order.calculatePrice(regularTickets, premiumTickets, WeekDay);

            Assert.Equal(22, res);

        }

        [Fact]
        public void Test_Regular_Order_On_Weekday_Returns_Correct_Value()
        {
            int regularTickets = 2;
            int premiumTickets = 2;

            Order order = new Order(2, false);

            double res = order.calculatePrice(regularTickets, premiumTickets, WeekDay);

            Assert.Equal(23, res);

        }

        [Fact]
        public void Test_Regular_Order_Under_6_In_Weekend_Returns_Correct_Value()
        {
            int regularTickets = 2;
            int premiumTickets = 2;

            Order order = new Order(3, false);

            double res = order.calculatePrice(regularTickets, premiumTickets, Weekend);

            Assert.Equal(46, res);

        }

        [Fact]
        public void Test_Regular_Order_Above_6_In_Weekend_Returns_Correct_Value()
        {
            int regularTickets = 6;
            int premiumTickets = 0;

            Order order = new Order(4, false);

            double res = order.calculatePrice(regularTickets, premiumTickets, Weekend);

            Assert.Equal(54, res);

        }

    }
}
