﻿namespace soa3_project.Domain
{
    public enum TicketExportFormat
    {
        PLAINTEXT, 
        JSON
    }
}