﻿using System;
using System.Collections.Generic;
using System.Text;

namespace soa3_project.Domain
{
    class MovieScreening
    {
        public DateTime date;

        public double pricePerSeat { get; }

        public MovieScreening(Movie movie, DateTime date, double pricePerSeat)
        {
            this.date = date;
            this.pricePerSeat = pricePerSeat;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
