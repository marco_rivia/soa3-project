﻿using soa3_project.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace soa3_project
{
    class Order
    {
        public int orderNr { get; }

        public bool isStudentOrder;

        public Order(int orderNr, bool isStudentOrder)
        {
            this.orderNr = orderNr;
            this.isStudentOrder = isStudentOrder;
        }

        public void addSeatReservation(MovieTicket movieTicket)
        {
            
        }

        public double calculatePrice(int regularTickets, int premiumTickets, DateTime today)
        {
            double basePrice = 10;
            // Student -> every second ticket free
            if(isStudentOrder)
            {
                double regularPrice = Math.Ceiling((double)regularTickets / 2) * basePrice;
                double premiumPrice = Math.Ceiling((double)premiumTickets / 2) * (basePrice + 2);
                return regularPrice + premiumPrice;
            }
            else
            {
                if (isWeekend(today))
                {
                    // Weekend for regular customers >= 6 tickets -> 10% discount
                    if(regularTickets + premiumTickets >= 6)
                    {
                        double regularPrice = 0.9 * (regularTickets * basePrice);
                        double premiumPrice = 0.9 * (premiumTickets * (basePrice + 3));
                        return regularPrice + premiumPrice;
                    }
                    // Weekend for regular customers < 6 tickets -> full price
                    else
                    {
                        double regularPrice = regularTickets * basePrice;
                        double premiumPrice = premiumTickets * (basePrice + 3);
                        return regularPrice + premiumPrice;
                    }
                }
                // Weekday for regular customers -> every second ticket free
                else
                {
                    double regularPrice = Math.Ceiling((double)regularTickets / 2) * basePrice;
                    double premiumPrice = Math.Ceiling((double)premiumTickets / 2) * (basePrice + 3);
                    return regularPrice + premiumPrice;
                }
            }
        }

        public void export(TicketExportFormat exportFormat)
        {
            switch(exportFormat)
            {
                case TicketExportFormat.JSON:
                    Console.WriteLine("json");
                    break;
                case TicketExportFormat.PLAINTEXT:
                    Console.WriteLine("plain text");
                    break;
                default:
                    Console.Write("type not supported");
                    break;
            }
        }

        private bool isWeekend(DateTime today)
        {
            return today.DayOfWeek == DayOfWeek.Friday || today.DayOfWeek == DayOfWeek.Saturday || today.DayOfWeek == DayOfWeek.Sunday;
        }

    }
}
