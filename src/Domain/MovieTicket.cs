﻿using System;
using System.Collections.Generic;
using System.Text;

namespace soa3_project.Domain
{
    class MovieTicket
    {
        public MovieTicket(MovieScreening movieScreening, int rowNr, int seatNr, bool isPremium)
        {
            this.rowNr = rowNr;
            this.seatNr = seatNr;
            this.isPremium = isPremium;
        }

        public int rowNr;

        public int seatNr;

        public bool isPremium { get; }

        public double getPrice()
        {
            return 10;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
