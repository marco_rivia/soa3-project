﻿using System;
using soa3_project.Domain;

namespace soa3_project
{
    class Program
    {
        static void Main(string[] args)
        {
            Order order = new Order(1, false);
            double res = order.calculatePrice(5,5, DateTime.Today);
            Console.Write(res);
        }
    }
}
